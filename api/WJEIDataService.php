<?php

interface WJEIDataService {
  public static function initWithNationalData();
  public static function initWithRegionalData($region);
  public static function initWithStateData($state);
}