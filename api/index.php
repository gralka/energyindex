<?php

require_once 'vendor/autoload.php';
require_once 'WJEIFileParser.php';

$app = new \Slim\App();

$app->get('/national', function () {
  header("Content-Type: application/json");
  WJEIFileParser::initWithNationalData();
  echo "<pre>";
  print json_encode(WJEIFileParser::$results->data, JSON_PRETTY_PRINT);
});

$app->get('/region', function(){
  header("Content-Type: application/json");

});

$app->get('/region/:region', function($region){
  header("Content-Type: application/json");

});

$app->get('/state', function(){
  header("Content-Type: application/json");

});

$app->get('/state/:state', function($state){
  header("Content-Type: application/json");

});

$app->run();
