<?php 

require_once 'WJEIDataService.php';

class WJEIFileParser implements WJEIDataService {

  public static $dataPath = 'data/';

  public static $labels;
  public static $results;

  public static function initWithNationalData() {
    $nationalDataPath = self::$dataPath . 'National/';
    self::loadValidLabels();

    $results = &self::$results;
    $results = new stdClass();
    unset($results->error);
    
    $results->data = array();
    try {
      $nationalCSVFiles = array_filter(scandir($nationalDataPath), "self::isNotDotDir");

      foreach ($nationalCSVFiles as $file) {
        $handle = fopen($nationalDataPath . $file, "r");
        $headingLabels = fgetcsv($handle);

        $dataset = self::parseDatasetLabel($file);

        $results->data[$dataset] = array();

        while(!feof($handle)){
          $line = fgetcsv($handle);

          if($dataset !== "energy_index" && $dataset !== "energy_index_with_exports") {
            $results->data[$dataset][$line[0]] = array_combine(
              array_slice($headingLabels, 1),
              array_map("doubleval", array_slice($line, 1))
            );
          }
          else {
            $results->data[$dataset][$line[0]] = array_map("doubleval", array_slice($line, 1))[0];
          }
        }
        
        fclose($handle);
      }
    }

    catch(Exception $e) {
      $results->data = null;
      $results->error = array('error' => array(
        'reason' => "There was an error reading the national W&J Energy Index data.",
        'exception_file' => $e->getFile(),
        'exception_line' => $e->getLine(),
        'exception_message' => $e->getMessage())
      );
    }
  }

  public static function initWithRegionalData($region) {
    #TODO: Implement regional data file parsing.
  }

  public static function initWithStateData($state) {
    #TODO: Implement state data file parsing.
  }

  // Helper Functions

  private static function parseDatasetLabel($file){
    $str = preg_replace("/W\&J\s|U\.S\.|\(|\)|Domestically|\-.*/", '', $file);
    $str = preg_replace("/\%/", 'percent', $str);
    $str = preg_replace("/\s\+/", 'with', $str);
    $str = preg_replace("/[Ee]xport(?!s)/", 'exports', $str);
    $str = preg_replace("/^\s|\s$/", '', $str);
    $str = preg_replace("/\s/", '_', $str);
    return mb_convert_case($str, MB_CASE_LOWER);
  }

  private static function loadValidLabels(){
    $labels = file_get_contents(self::$dataPath . "labels.json");
    self::$labels = json_decode($labels, true);
  }

  private static function isNotDotDir($item){
    return stripos($item, '.') !== 0;
  }

}
