(function($, undefined){

	// Geographical Region

	// Disable select elements by default
	$("#geographic_controls select").prop('disabled', true)
	
	// Toggle select elements for geographic reigon
	$("#geographic_national").on('click', function(){
		$("#geographic_controls select").prop('disabled', true)
	})

	$("#geographic_regional").on('click', function(){
		$("#geographic_controls select").prop('disabled', true)
		$("#region").prop('disabled', false)
	})

	$("#geographic_state").on('click', function(){
		$("#geographic_controls select").prop('disabled', true)
		$("#state").prop('disabled', false)
	})

	// Source Checkboxes - Select All/None
	$("#all_energy_sources").on('click', function(){
		$("#source_controls input[type=checkbox]").prop('checked', true)
	})

	$("#no_energy_sources").on('click', function(){
		$("#source_controls input[type=checkbox]").prop('checked', false)
	})
})(jQuery)