// Generated by CoffeeScript 1.10.0
(function() {
  (function($) {
    var current, displayControls, findTest, loadTest, showError, testCaseList, testData, testsPromise, updateCase;
    current = null;
    $("#controls").find("input, a").attr("disabled", "disabled");
    testData = null;
    testsPromise = $.getJSON('tests/tests.json', function(data) {
      return testData = data;
    });
    findTest = function(test_case) {
      var index;
      index = null;
      $.each(testData, function(i, data) {
        if (data.name === test_case) {
          index = i;
          return true;
        }
      });
      return index;
    };
    displayControls = function(index) {
      var key, results1, selector, sources, testControls;
      testControls = testData[index].controls;
      $("#start_year").val(testControls.date.start);
      $("#end_year").val(testControls.date.end);
      $("input[name=index_value]").prop('checked', false);
      $("#i" + testControls.i).prop('checked', true);
      $("input[name=geographic]").prop('checked', false);
      $("#geographic_" + (testControls.geography.option.toLowerCase())).prop("checked", true);
      $("#region, #state").find("option[value=blank]").prop('selected', true);
      switch (testControls.geography.option) {
        case "Regional":
          $("#region").find("option[value=" + testControls.geography.region + "]").prop("selected", true);
          break;
        case "State":
          $("#state").find("option[value=" + testControls.geography.region + "]").prop("selected", true);
          break;
      }
      sources = testControls.sources;
      results1 = [];
      for (key in sources) {
        selector = key.toLowerCase().replace(/\s|\//, '_');
        results1.push($("#" + selector).prop("checked", sources[key]));
      }
      return results1;
    };
    loadTest = function(index) {
      var loadPromise, results, testDescription, testFile;
      testDescription = testData[index].description;
      testFile = testData[index].result;
      results = null;
      loadPromise = $.get("tests/" + testFile, function(data) {
        return results = data;
      });
      loadPromise.done(function() {
        displayControls(index);
        return $("#results-data").html(results);
      });
      loadPromise.fail(function() {
        return $("#results-data").html("<p>No test data found for " + testData[index].name + ".</p>");
      });
      return loadPromise.always(function() {
        return $("#results-title").html("" + testDescription);
      });
    };
    showError = function(index) {
      $("#results-data").html("<p>No test found for " + index + ".</p>");
      return $("#results-title").html("Test Case Not Found");
    };
    updateCase = function() {
      var hash;
      hash = window.location.hash;
      if (hash.length !== 0) {
        hash = hash.substr(1, hash.length);
      } else {
        hash = 'case_1';
      }
      current = findTest(hash);
      if (current !== null) {
        return loadTest(current);
      } else {
        return showError(hash);
      }
    };
    $(window).bind("hashchange", updateCase);
    testCaseList = $("#test-case-list");
    return testsPromise.done(function() {
      $.each(testData, function(i, data) {
        testCaseList.append("<li><a href=#" + data.name + ">" + data.description + "</a></li>");
        return updateCase();
      });
      $("#show_hide_controls").css('float', 'right');
      return $(".show_hide_controls_container").click(function() {
        return $("#controls").slideToggle('slow', "easeInOutQuart");
      });
    });
  })(jQuery);

}).call(this);
