(($) ->
  current = null

  # Prevent Control Clicking
  $("#controls")
    .find("input, a")
    .attr "disabled", "disabled"

  # Load Tests File
  testData = null
  testsPromise = $.getJSON 'tests/tests.json', (data) -> testData = data

  # Given a name, retrieve the test's index in the testData array.
  findTest = (test_case) ->
    index = null
    $.each testData, (i, data) ->
      if data.name is test_case
        index = i
        return true
    index

  # When a new case is selected, display the proper control configurations.
  displayControls = (index) ->
    testControls = testData[index].controls
    
    # Dates
    $("#start_year").val testControls.date.start
    $("#end_year").val testControls.date.end 

    # I Value
    $("input[name=index_value]").prop 'checked', false
    $("#i#{testControls.i}").prop 'checked', true

    # Geographic Region
    $("input[name=geographic]").prop 'checked', false
    $("#geographic_#{testControls.geography.option.toLowerCase()}").prop "checked", true

    # Geographic Region - Region/State Selection
    $("#region, #state").find("option[value=blank]").prop 'selected', true
    switch testControls.geography.option
      when "Regional"
        $("#region")
          .find("option[value=#{testControls.geography.region}]")
          .prop "selected", true
        break
      when "State"
        $("#state")
          .find("option[value=#{testControls.geography.region}]")
          .prop "selected", true
        break

    # Energy Sources
    sources = testControls.sources
    for key of sources
      selector = key.toLowerCase().replace(/\s|\//, '_')
      $("##{selector}").prop "checked", sources[key]

  # When a new case is selected, load the results into the view.
  loadTest = (index) ->    
    testDescription = testData[index].description
    testFile = testData[index].result
    results = null
    loadPromise = $.get "tests/#{testFile}", (data) -> results = data
    loadPromise.done () ->
      displayControls index
      $("#results-data").html results
    loadPromise.fail () ->
      $("#results-data").html "<p>No test data found for #{testData[index].name}.</p>"
    loadPromise.always () ->
      $("#results-title").html "#{testDescription}"

  # Display an error stating that the requested test case is not found.
  showError = (index) ->
    $("#results-data").html "<p>No test found for #{index}.</p>"
    $("#results-title").html "Test Case Not Found"
  
  # When update the test case.
  updateCase = () ->
    hash = window.location.hash
    if hash.length isnt 0
      hash = hash.substr(1, hash.length)
    else
      hash = 'case_1'

    current = findTest hash

    if current isnt null
      loadTest current
    else
      showError hash
  
  # Register hashchange listner to update on test case changes.
  $(window).bind "hashchange", updateCase

  # Load test case links into the navigation menu. 
  testCaseList = $("#test-case-list")
  testsPromise.done () ->
    $.each testData, (i, data) ->
      testCaseList.append("<li><a href=##{data.name}>#{data.description}</a></li>")
      updateCase()
    $("#show_hide_controls").css 'float', 'right'
    $(".show_hide_controls_container").click () ->
      $("#controls").slideToggle('slow', "easeInOutQuart")
) jQuery