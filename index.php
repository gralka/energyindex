<!DOCTYPE html> 
<html lang="en">
	<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>W&amp;J Energy Index</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="style.css">
		<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php
			require_once 'includes/navbar.html';
			require_once 'includes/controls.html';
		?>

		<div id="results" class="container">
			<h2 id="results-title"></h2>
			<div id="results-data"></div>
		</div>

		<script src="js/controls.js"></script>
		<script src="js/tests.js"></script>
	</body>
</html>